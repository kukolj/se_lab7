import BaseHTTPServer
from case_handlers import *
import os
# TODO:
# - handle errors 
#   - 404 for file not found
#   - 501 for features not yet implemented
#   - 500 for all other errors
# - handle multiple files
# - handle different mime types
# - move handlers to other class
# - create simple config file
# - error log i access log

class MySimpleServer(BaseHTTPServer.BaseHTTPRequestHandler):
    CASES = [
            CaseRootPathHandler(),
            CaseFileExistsHandler(), 
            CaseFileNotExistsHandler(),
            ]
    
    # Obradi GET request
    def do_GET(self):
        try:
            file_path = self.full_file_path()
            print "File: ", file_path
            for case in self.CASES:
                if case.test(file_path):
                    self.send_content(case.run(file_path))
                    break
        except CaseError as e:
            self.handle_error(e.error_code, e.message)

    def full_file_path(self):
        WWW_PATH = "/home/osirv/pi_labs/kukolj/se_lab7/www"
        return WWW_PATH + self.path
        return os.path.join(os.getcwd(), "www", self.path[1:])


    def handle_error(self, code, msg):
        self.send_response(code)
        self.send_header("Content-Type", "text/html")
        page = "<html><body><p>"+str(msg)+"</p></body></html>"
        self.send_header("Content-Lenght", str(len(page)))
        self.end_headers()
        self.wfile.write(page)


    def send_content(self, content):
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Lenght", str(len(content)))
        self.send_header("Cache-Control", "no-cache")
        self.end_headers()
        self.wfile.write(content)




if __name__ == '__main__':
    print "Starting web server..."
    serverAddress = ('', 8080)
    server = BaseHTTPServer.HTTPServer(
                                serverAddress, MySimpleServer)
    server.serve_forever()




